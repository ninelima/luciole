/*
  Universal 8bit Graphics Library (https://github.com/olikraus/u8g2/)
   Wiki  (https://github.com/olikraus/u8g2/wiki)
*/

/*
 SD card datalogger
 
 This example shows how to log data from three analog sensors to an SD card using the SD library.
 	
 The circuit:
 SD card attached to SPI bus as follows:
 *** UNO:  MOSI - pin 11, MISO - pin 12, CLK - pin 13, CS - pin 4 (CS pin can be changed) and pin #10 (SS) must be an output

  Mega:  MOSI - pin 51, MISO - pin 50, CLK - pin 52, CS - pin 4 (CS pin can be changed) and pin #52 (SS) must be an output
  Leonardo: Connect to hardware SPI via the ICSP header Pin 4 used here for consistency with other Arduino examples
 */

/*
MAX6675
1. GND
2. VCC
3. CLK
4. SO (Serial Out / Data)

LCD:
RS = CS
EN = CLK
RW = SI (Serial In / Data)

Breadbord:
Yellow = CLK
Orange = SO (data)
Blue =   MOSI
Orange = MISO

Netlist:
VCC     UNO.1.GND MAX6675.1.2  MAX6675.2.2   LCD.1.VCC
GND     UNO.1.5V  MAX6675.1.1  MAX6675.2.1   LCD.1.GND
CLK     UNO.1.13  MAX6675.1.3  MAX6675.2.3   LCD.1.E   (yellow)
MISO    UNO.1.12  MAX6675.1.5  MAX6675.2.5  
MOSI    UNO.1.12  LCD.1.RW    (blue)        
CS-LCD  UNO.1.9   LCD.1.RS
CS-MAX6675-1  UNO.1.8  MAX6675.1.4 (white)
CS-MAX6675-2  UNO.1.7  MAX6675.2.4 (green)
TACHO1  UNO.1.2
TACHO2  UNO.1.3
*/


#include <Arduino.h>
#include <U8g2lib.h>
#include <RTClib.h>
//#include <SdFat.h>
#include "sdfat-datalogger.h"

#ifdef U8X8_HAVE_HW_SPI
#include <SPI.h>
#endif
#ifdef U8X8_HAVE_HW_I2C
#include <Wire.h>
#endif

//#include <SD.h>
#include "max6675.h" // max6675.h file is part of the library that you should download from Robojax.com


/*
  U8g2lib Example Overview:
    Frame Buffer Examples: clearBuffer/sendBuffer. Fast, but may not work with all Arduino boards because of RAM consumption
    Page Buffer Examples: firstPage/nextPage. Less RAM usage, should work with all Arduino boards.
    U8x8 Text Only Example: No RAM usage, direct communication with display controller. No graphics, 8x8 Text only.
    
*/
#include "constructor.h"
#include "Engine.h"

#define TIMER 500      // Timer interval 500
// #define _PORTRAIT_  // landscape will be used for now. May revisit this later

// Define SPI devices vars
#define MOSI 11
#define MISO 12
#define CLK  13

#define CS_TC_2  6   //OilT 
#define CS_TC_1  7   //CHT2
#define CS_TC_0  8   //CHT1
#define CS_LCD   9   //LCD Display

//U8G2_ST7920_128X64_F_SW_SPI(const u8g2_cb_t *rotation, uint8_t clock, uint8_t data, uint8_t cs, uint8_t reset = U8X8_PIN_NONE)
#ifdef _PORTRAIT_
  #define LRPMX 162
  #define MRPMX 20
  #define OILX 30
  #define SCALE_LEN 50
  #define OILPY 18  //19
  #define OILTY 33  //34
  U8G2_ST7920_128X64_F_SW_SPI u8g2(U8G2_R1, /*clock=*/ 13, /*data MOSI*/ 11, /*CS=*/ CS_LCD);   // portrait - reset not connected
#else
  #define LRPMX 62
  #define MRPMX 83
  #define OILX 30
  #define SCALE_LEN 50
  #define OILPY 18  //19
  #define OILTY 33  //34
  U8G2_ST7920_128X64_F_SW_SPI u8g2(U8G2_R0, /*clock=*/ 13, /*data MOSI*/ 11, /*CS=*/ CS_LCD);   // landscape - reset not connected
#endif

// MAX6675 max6675_0(pinSCK0, pinCS0, pinSO0);    // create instance object of MAX6675
MAX6675 max6675_2(/*clock*/ 13,  /*CS=*/ CS_TC_2, /*data MISO*/ 12); // OilT object of MAX6675  
MAX6675 max6675_1(/*clock*/ 13,  /*CS=*/ CS_TC_1, /*data MISO*/ 12); // CHT2 object of MAX6675
MAX6675 max6675_0(/*clock*/ 13,  /*CS=*/ CS_TC_0, /*data MISO*/ 12); // CHT1 object of MAX6675 

Engine engine;

// Tacho vars
#define INTERRUPT_PIN 3  // Only 2 or 3 are interrupt pins 
unsigned long _millis = 0;
volatile double g_revticks = 0;
unsigned int g_ticks = 0;   

//-----------------------------------------------------------------------------
// Tacho interrupt service routine
//
void isr() 
{
  g_revticks = g_revticks + 1;
}

void attachInterrupt()
{
  attachInterrupt(digitalPinToInterrupt(INTERRUPT_PIN), isr, RISING);  // pin 3 - RISING, FALLING, CHANGE
}

void detachInterrupt()
{
  detachInterrupt(digitalPinToInterrupt(INTERRUPT_PIN));  // pin 3
}

//-----------------------------------------------------------------------------
// u8g2 - Graphics and Fonts
//

void u8g2_prepare(void) 
{
  //u8g2.setFont(u8g2_font_6x10_tf);  
  u8g2.setFontRefHeightExtendedText();
  u8g2.setDrawColor(1);
  u8g2.setFontPosTop();
  u8g2.setFontDirection(0);  //1 = portrait
}

// https://support.michiganavionics.com/portal/en/kb/articles/aerovee-engine-sensor-setup

void u8g2_page_1() 
{
  u8g2.setFont(u8g2_font_6x10_tr);   // 9 pix  6x13   u8g2_font_6x12_mr

  u8g2.drawStr(LRPMX, 0, "RPM");
  u8g2.drawStr(0, OILPY - 1, "OilP");
  u8g2.drawStr(0, OILTY - 1, "OilT");
  u8g2.drawStr(0, 54, "CHT");

  //u8g2.drawStr(25, 0, F("V");  // V same size as RPM
  //u8g2.setFont(u8g2_font_6x10_mf);  // 6x10

  u8g2.setFont(u8g2_font_5x8_mf);  // 6x10
  u8g2.drawStr(25, 0, "V");  
  u8g2.drawStr(110, 20, "bar");
  u8g2.drawUTF8(110, 36, "°C");  // OilT
  //u8g2.drawUTF8(110, 54, F("°C");  // CHT - remove to make space for Hobbs

  // Oil P
  u8g2.drawFrame(OILX, OILPY, SCALE_LEN, 7);
  u8g2.drawFrame(OILX, OILPY, 8, 7);  // min green
  u8g2.drawFrame(72,   OILPY, 8, 7);  // max green  130 degC
  
  // OilT
  u8g2.drawFrame(OILX, OILTY, SCALE_LEN, 7);
  u8g2.drawFrame(OILX, OILTY, 8, 7);  // no real lower limit for OilT
  u8g2.drawFrame(72, OILTY, 8, 7);    // max green

  for (int i = 2; i < 6; i += 2) {
    // OilP
    u8g2.drawPixel(OILX + i, OILPY + 2);
    u8g2.drawPixel(OILX + i + 1, OILPY + 4);
    u8g2.drawPixel(72 + i, OILPY + 2);
    u8g2.drawPixel(72 + i +1, OILPY + 4);

    // OilT
    u8g2.drawPixel(30 + i, 36);   // no real lower limit
    u8g2.drawPixel(31 + i, 38);   // no real lower limit
    u8g2.drawPixel(72 + i, OILTY + 2);
    u8g2.drawPixel(72 + i + 1, OILTY + 4);
  }
}

void u8g2_page_1_display_data() 
{
  // RPM
  u8g2.setFont(u8g2_font_chargen_92_mn);  /// monospaced 14 pix font
  u8g2.drawStr(MRPMX-2, 0, engine.getRPM());

  // Volts
  u8g2.setFont(u8g2_font_helvB08_tn);  //  pix height  10
  u8g2.drawStr(0, 0, engine.getVolt());

  // Oil P&T 
  //u8g2.setFont(u8g2_font_helvB08_tf);  //  u8g2_font_helvB08_tf  u8g2_font_helvR08_tf

  // Oil Pressure - bar (0-8 bar) red < 1, > 7
  //if (engine.mOilP < engine.lOilP) u8g2.setDrawColor(0);  // 0 = invert
  u8g2.drawStr(90, 18, engine.getOilP());
  u8g2.setDrawColor(1);  // 0 = invert
  // OilP needle
  int i = 30 + (SCALE_LEN * engine.mOilP / 8) ;
  //u8g2.drawBox(50, 16, 3, 13);
  u8g2.drawBox(i, OILPY - 3, 3, 13);

  //Oil Temp - deg C (50 - 150)  green 70  - 130 degC. Red > 130 
  u8g2.drawStr(90, 33, engine.getOilT());
  // OilT needle
  i = 30 + (SCALE_LEN * (engine.mOilT - 50) / 100) ;
  //u8g2.drawBox(55, 31, 3, 13);
  u8g2.drawBox(i, OILTY - 3, 3, 13);

  // CHT - 50 - 150 deg C
  // SCALE 100 - 150
  // High alarm = 150
  // High caution = 130
  u8g2.setFont(u8g2_font_5x8_mf);  // 6x10

  //CHT1 bargraph
  int barx;
  barx = 25;
  draw_cht_bargraph(engine.mCHT1, 25);
  u8g2.drawStr(barx + 13, 51, engine.getCHT1());

  barx = 60;
  draw_cht_bargraph(engine.mCHT2, barx);
  u8g2.drawStr(barx + 13, 51, engine.getCHT2());
}

//CHT1 bargraph
// in degC
#define EGT_LO_RANGE 100 //50
#define EGT_HI_RANGE 150
#define EGT_HI_WARN  150
#define EGT_HI_CAUTION 125
// in pixels
#define EGT_FULLSCALE 23
#define EGT_CAUTION EGT_FULLSCALE*(EGT_HI_CAUTION-EGT_LO_RANGE)/(EGT_HI_RANGE-EGT_LO_RANGE)

void draw_cht_bargraph(int cht, int barx)
{
  int step = 2;
  int scaleCHT;

  if ((cht > EGT_HI_WARN) && (g_ticks % 2 == 0)) {
    ;  // for flashing
  }
  else {
    scaleCHT = min(EGT_FULLSCALE * (cht - EGT_LO_RANGE) / (EGT_HI_RANGE - EGT_LO_RANGE), EGT_FULLSCALE);
    for (int i = 0; i < scaleCHT; i+=step) {
      u8g2.drawLine(barx, 64 - i, barx + 10, 64 - i);
      if (i < EGT_CAUTION) step = 2; else step = 1;
    }
  }
}

#define TIMERX 50
#define TIMERY 11

void u8g2_page_2()
{
  u8g2.setFont(u8g2_font_6x10_tr);   // 9 pix  6x13   u8g2_font_6x12_mr
  //u8g2.setFont(u8g2_font_helvB08_tf);  // pix height  10
  u8g2.drawStr(46, 0, "TIMERS");
  u8g2.setFont(u8g2_font_6x10_tr);   // 9 pix  6x13   u8g2_font_6x12_mr
  u8g2.drawStr(0, 1 * TIMERY, "UTC");
  u8g2.drawStr(0, 2 * TIMERY, "LOCAL");
  u8g2.drawStr(0, 3 * TIMERY, "FLT");
  u8g2.drawStr(0, 4 * TIMERY, "HOBBS");
  u8g2.drawStr(0, 5 * TIMERY, "MAINT");
}

void u8g2_page_2_display_data()
{
  u8g2.setFont(u8g2_font_helvB08_tf);  //  pix height  10
  u8g2.drawStr(TIMERX, 1 * TIMERY, engine.getUTC());  // 
  u8g2.drawStr(TIMERX, 2 * TIMERY, engine.getLOCAL());  //"08:12"
  u8g2.drawStr(TIMERX, 3 * TIMERY, engine.getFLT());   //"01:40"
  u8g2.drawStr(TIMERX, 4 * TIMERY, engine.getHobbs());  //"10.1");
  u8g2.drawStr(TIMERX, 5 * TIMERY, engine.getMaint());  //"150 Hr");
}

void disable_spi_pin(int8_t pin)
{
    Serial.print(F("Disabling SPI device on pin "));
    Serial.println(pin);
    pinMode(pin, OUTPUT); digitalWrite(pin, HIGH);
}


uint8_t draw_state = 0;

void draw(void) 
{
  u8g2_prepare(); //???

  ///*
  u8g2_page_1(); 
  u8g2_page_1_display_data();
  //*/

  /*
  u8g2_page_2(); 
  u8g2_page_2_display_data();
  //*/
}

// On the Ethernet Shield, CS is pin 4. Note that even if it's not
// used as the CS pin, the hardware CS pin (10 on most Arduino boards,
// 53 on the Mega) must be left as an output or the SD library
// functions will not work.
//const int chipSelect = 10;
//File dataFile;

void setup(void) 
{
  Serial.begin(9600);
  u8g2.begin();
  attachInterrupt();
 }

void loop(void) 
{
  unsigned long elaps_time = millis() - _millis;

  if (elaps_time >= TIMER) {
    noInterrupts();     // start vital section

    // Calc the revs
    engine.mRPM  = max((g_revticks * 60000 ) / elaps_time, 0);   // calculates rpm

    //Serial.print(" revticks = ");    Serial.print(g_revticks);
    //Serial.print(" RPM = ");    Serial.println(engine.mRPM);

    
    engine.mCHT1 = (int) max6675_0.readCelsius();  // in deg C  
    engine.mCHT2 = (int) max6675_1.readCelsius();  // in deg C  
    engine.mOilT = (int) max6675_2.readCelsius();  // in deg C  

    // For debug
    engine.mOilT = (engine.mCHT1 + engine.mCHT1);  // in deg C  -- for debug just add the two CHT's

    /*Serial.print("C1 = ");     Serial.print(engine.mCHT1);
    Serial.print(" C2 = ");    Serial.print(engine.mCHT2);
    Serial.println();*/

    // Update the display
    u8g2.clearBuffer();
    draw();  
    u8g2.sendBuffer();

    // End of TIMER slice
    g_ticks++;

    // Update every 60 TIMER periods. for TIMER 500, this means every 30 seconds. 
    //if (g_ticks % 60 == 0) {
    /*
    if (g_ticks % 10 == 0) {
      interrupts();  // end vital section
      disable_spi_pin(CS_LCD);   //LCD
      disable_spi_pin(CS_TC_0);  //CHT 1
      disable_spi_pin(CS_TC_1);  //CHT 2
      disable_spi_pin(CS_TC_2);  //OilT

      for (int pin = 6; pin < 10; pin++) {
        pinMode(pin, OUTPUT); digitalWrite(pin, HIGH);
      }

      Serial.println("\nsd_setup");     
      sd_setup();
      sd_log_data() ;
    }
    //*/

    Serial.print(".");     

		g_revticks = -1;  
    _millis = millis();
    interrupts();  // end vital section
  }
}

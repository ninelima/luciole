//#ifndef ARDUINO_AVR_UNO
 #include <avr/dtostrf.h>  // include for Arduino M0 Pro
//#endif


class Engine
{
  private:
  char buf[12];

  public:
  // Measured values
  long  mRPM;
  float mVolt;
  float mOilP;
  int   mOilT;
  int   mCHT1, mCHT2;

  // Time values


  // Limits
  float lOilP = 1;
  float hOilP = 7;
  int lOilT = 60;  // green
  int hOilT = 100; // green // red 120
  float lVolt = 10.5;
  float hVolt = 14.5;
  //int lCHT = 
  //int hCHT = 
  // RPM 3600 // redline

  Engine()
  {
    mRPM = 0; //4321;
    mVolt = 0; //13.1;
    mOilP = 0; //5.4;
    mOilT = 0; //96;
    mCHT1 = 0; //123;
    mCHT2 = 0; //80;
  } 

  void sim()
  {
    
    if (rand() < RAND_MAX / 2) mRPM += 1;    if (mRPM > 4400) mRPM = 0;
    if (rand() < RAND_MAX / 1) mVolt += 0.1; if (mVolt > 14) mVolt = 0;
    if (rand() < RAND_MAX / 3) mOilP += 0.1; if (mOilP > 8) mOilP = 0;
    //if (rand() < RAND_MAX / 2) mOilT += 1;   if (mOilT > 150) mOilT = 50;
    if (rand() < RAND_MAX / 2) mCHT1 += 1;   if (mCHT1 > 180) mCHT1 = 50;
    //if (rand() < RAND_MAX / 3) mCHT2 += 1;   if (mCHT2 > 180) mCHT2 = 40;
  }  

  char *getRPM() 
  {
    sprintf(buf, "%04u", mRPM/10*10);  // to the nearest 10. Ie last digit is always 0
    return buf;
    //return itoa((mRPM/10)*10, buf, 10);
  }

  char *getVolt() 
  {
    //sprintf(buf, "%3.1f", mVolt);
    //return buf;
    return dtostrf(mVolt, 1, 1, buf);
  }

  char *getOilP() 
  {
    //dtostrf(float_value, min_width, num_digits_after_decimal, where_to_store_string)
    return dtostrf(mOilP, 1, 1, buf);
  }

  char *getOilT() 
  {
    //sprintf(buf, "%u", mOilT);
    //return buf;
    return itoa(mOilT, buf, 10);
  }

  char *getCHT1() 
  {
    //sprintf(buf, "%u", mCHT1);
    //return buf;
    return itoa(mCHT1, buf, 10);
  }

  char *getCHT2() 
  {
    //sprintf(buf, "%u", mCHT2);
    //return buf;
    return itoa(mCHT2, buf, 10);
  }

  char *getUTC() 
  {
    //sprintf(buf, "%u", mCHT2);
    //return buf;
    return itoa(mCHT2, buf, 10);
  }


  char *getLOCAL() {}
  char *getFLT() {}
  char *getHobbs() {}
  char *getMaint() {}

};
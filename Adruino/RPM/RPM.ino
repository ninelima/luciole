/*
 A set of custom made large numbers for a 16x2 LCD using the 
 LiquidCrystal librabry. Works with displays compatible with the 
 Hitachi HD44780 driver. 
 
 The Circuit:
 LCD RS pin to D12
 LCD Enable pin to D11
 LCD D4 pin to D5
 LCD D5 pin to D4
 LCD D6 pin to D3
 LCD D7 pin to D2
 LCD Vee tied to a pot to control brightness
 LCD Vss and R/W tied to ground
 LCD Vcc to +5V

 Ronivaldo Sampaio (ronivaldo@gmail.com) for 3 lines
 2013/05/15 -> change for character with only 3 lines
 Original Made by Michael Pilcher
 2/9/2010
 
 character created using:
 http://mikeyancey.com/hamcalc/lcd_characters.php
 */

// include the library
#include <LiquidCrystal.h>
#include <Adafruit_BMP085.h>
#include <Wire.h>
#include "udc.h"

#include "max6675.h" // max6675.h file is part of the library that you should download from Robojax.com

#define INTERRUPT_PIN 3  // Only 2 or 3 are interrupt pins 

// Thermocouple MAX6675
#define pinCS0 11  // CS = chip select CS pin
#define pinSO0 12  // SO=Serial Out
#define pinSCK0 13 // SCK = Serial Clock pin

// MAX6675(int8_t SCLK, int8_t CS, int8_t MISO);
// MAX6675 max6675_0(pinSCK0, pinCS0, pinSO0);    // create instance object of MAX6675
MAX6675 max6675_0(13, 11, 12); // create instance object of MAX6675
MAX6675 max6675_1(13, 2,  12); // create instance object of MAX6675


// Adafruit 16x2 LCD Keypad
// Button definitions
#define btnRIGHT  0
#define btnUP     1
#define btnDOWN   2
#define btnLEFT   3
#define btnSELECT 4
#define btnNONE   5

#define TIMER 500 //500   // Timer interval

// Create the objects
//LiquidCrystal lcd(12, 11, 5, 4, 3, 2);
LiquidCrystal lcd(8, 9, 4, 5, 6, 7);        // select the pins used on the LCD panel

// Adafruit Pressure sensor
Adafruit_BMP085 bmp;                        

int i = 0; 
int state = 1;        // State machine "state" variable
int MAX_STATE = 3;    // Max number of states for the state machine 


// define some values used by the panel and buttons
int  lcd_key    = 0;
int  _lcd_key   = 0;   // previous key
int  adc_key_in  = 0;
bool lcd_key_pressed  = false;

// Tacho vars
//volatile unsigned int g_revticks = 0;
unsigned long _millis = 0;
volatile double g_revticks = 0;

// Global vars
unsigned int g_rpm;
int g_altitude;
int g_oat;
int g_cht;
int g_egt;
// Previous values
//int _g_altitude;
//int _time = 0;  // previous time


//-----------------------------------------------------------------------------
// Define the User Defined Characters (UDC)
//
void defineUDC_0()
{
	// sets the LCD's rows and colums:
	lcd.begin(16, 2);

	// assigns each segment a write number
	lcd.createChar(1, seg1);
	lcd.createChar(2, seg2);
	lcd.createChar(3, seg3);
	lcd.createChar(4, seg4);
	lcd.createChar(5, seg5);
	lcd.createChar(6, seg6);
	lcd.createChar(7, seg7);
	lcd.createChar(8, seg8);
}

void defineUDC_1()
{
	// sets the LCD's rows and colums:
	lcd.begin(16, 2);

	// assigns each segment a write number
	lcd.createChar(1, bar1);
	lcd.createChar(2, bar2);
	lcd.createChar(3, bar3);
	lcd.createChar(4, bar4);
	lcd.createChar(5, bar5);
	lcd.createChar(6, bar6);
	lcd.createChar(7, bar7);
	lcd.createChar(8, bar8);
}

//-----------------------------------------------------------------------------
// Read the buttons
//

int read_LCD_buttons()
{
	// V1.1
	if (false) {
		adc_key_in = analogRead(0);      // read the value from the sensor 

		// my buttons when read are centered at these valies: 0, 144, 329, 504, 741
		// we add approx 50 to those values and check to see if we are close
		if (adc_key_in > 1000) return btnNONE; // We make this the 1st option for speed reasons since it will be the most likely result
		// For V1.1 and later use this threshold. 
		// for older see http://www.dfrobot.com/wiki/index.php?title=Arduino_LCD_KeyPad_Shield_%28SKU:_DFR0009%29
		if (adc_key_in < 50)   return btnRIGHT;  
		if (adc_key_in < 250)  return btnUP; 
		if (adc_key_in < 450)  return btnDOWN; 
		if (adc_key_in < 650)  return btnLEFT; 
		if (adc_key_in < 850)  return btnSELECT;  
	}

	// V1.0
	if (true) {
		adc_key_in = analogRead(0);      // read the value from the sensor 

		if (adc_key_in > 1000) return btnNONE; // We make this the 1st option for speed reasons since it will be the most likely result
		if (adc_key_in < 50)   return btnRIGHT;  
		if (adc_key_in < 150)  return btnUP; 
		if (adc_key_in < 300)  return btnDOWN; 
		if (adc_key_in < 450)  return btnLEFT; 
		if (adc_key_in < 700)  return btnSELECT;  
	}

	return btnNONE;  // when all others fail, return this...
}


void echoButton()
{
  lcd.setCursor(0, 0);
  lcd.print("STATE: ");
  lcd.print(state);

	lcd.setCursor(10, 0);          // move cursor to second line "1" and 9 spaces over
	lcd.print(millis()/1000);      // display seconds elapsed since power-up or reset
  lcd.print("ms");
	lcd.setCursor(0,1);            // move to the beginning of the second line
	lcd_key = read_LCD_buttons();  // read the buttons

	 switch (lcd_key)              // depending on which button was pushed, we perform an action
	 {
	   case btnRIGHT:
		 {
		 lcd.print("RIGHT ");
		 break;
		 }
	   case btnLEFT:
		 {
		 lcd.print("LEFT   ");
		 break;
		 }
	   case btnUP:
		 {
		 lcd.print("UP    ");
		 break;
		 }
	   case btnDOWN:
		 {
		 lcd.print("DOWN  ");
		 break;
		 }
	   case btnSELECT:
		 {
		 lcd.print("SEL   ");
		 break;
		 }
	   case btnNONE:
		 {
		 lcd.print("NONE  ");
		 break;
		 }
	 }
	lcd.print(adc_key_in);
}


void custom0(int col)
{ 
	// uses segments to build the number 0
  lcd.setCursor(col, 0); 
  lcd.write(2);  
  lcd.write(8); 
  lcd.write(1);  
  lcd.setCursor(col, 1); 
  lcd.write(2);  
  lcd.write(6);  
  lcd.write(1);  
}

void custom1(int col)
{
  lcd.setCursor(col,0);
  lcd.write(0x20);
  lcd.write(0x20);
  lcd.write(1);
  lcd.setCursor(col,1);
  lcd.write(0x20);
  lcd.write(0x20);
  lcd.write(1);
}

void custom2(int col)
{
  lcd.setCursor(col,0);
  lcd.write(5);
  lcd.write(3);
  lcd.write(1);
  lcd.setCursor(col, 1);
  lcd.write(2);
  lcd.write(6);
  lcd.write(6);  //(0x2e);  
}

void custom3(int col)
{
  lcd.setCursor(col,0);
  lcd.write(5);
  lcd.write(3);
  lcd.write(1);
  lcd.setCursor(col, 1);
  lcd.write(7);
  lcd.write(6);
  lcd.write(1); 
}

void custom4(int col)
{
  lcd.setCursor(col,0);
  lcd.write(2);
  lcd.write(6);
  lcd.write(1);
  lcd.setCursor(col, 1);
  lcd.write(0x20);
  lcd.write(0x20);
  lcd.write(1);
}

void custom5(int col)
{
  lcd.setCursor(col,0);
  lcd.write(2);
  lcd.write(3);
  lcd.write(4);
  lcd.setCursor(col, 1);
  lcd.write(7);
  lcd.write(6);
  lcd.write(1);
}

void custom6(int col)
{
  lcd.setCursor(col,0);
  lcd.write(2);
  lcd.write(3);
  lcd.write(4);
  lcd.setCursor(col, 1);
  lcd.write(2);
  lcd.write(6);
  lcd.write(1);
}

void custom7(int col)
{
  lcd.setCursor(col,0);
  lcd.write(2);
  lcd.write(8);
  lcd.write(1);
  lcd.setCursor(col, 1);
  lcd.write(0x20);
  lcd.write(0x20);
  lcd.write(1);
}

void custom8(int col)
{
  lcd.setCursor(col, 0); 
  lcd.write(2);  
  lcd.write(3); 
  lcd.write(1);
  lcd.setCursor(col, 1); 
  lcd.write(2);  
  lcd.write(6);  
  lcd.write(1);
}

void custom9(int col)
{
  lcd.setCursor(col, 0); 
  lcd.write(2);  
  lcd.write(3); 
  lcd.write(1);
  lcd.setCursor(col, 1); 
  lcd.write(7);  
  lcd.write(6);  
  lcd.write(1);
}

void printNumber(int value, int col) 
{
 	switch (value) {
		case 0: custom0(col);  break;
		case 1: custom1(col);  break;
		case 2: custom2(col);  break;
		case 3: custom3(col);  break;
		case 4: custom4(col);  break;
		case 5: custom5(col);  break;
		case 6: custom6(col);  break;
		case 7: custom7(col);  break;
		case 8: custom8(col);  break;
		case 9: custom9(col);  break;
	}
}  

void printRPM(int rpm) 
{
  int m, c, d, u, number;

  number = rpm;
  if (number > 999) {
    m = (number - (number % 1000)) / 1000;
    number = number % 1000;
  } 
  else {
    m = 0;
  }  
  
  if (number > 99) {
    c = (number - (number % 100)) / 100;
    number = number % 100;
  } 
  else {
    c = 0;
  }  

  if (number > 9) {
    d = (number - (number % 10)) / 10;
    number = number % 10;
  } 
  else {
    d = 0;
  }  
  
  u = number;
  
  lcd.setCursor(0, 0);
  lcd.print("12V");

  lcd.setCursor(0, 1);
  lcd.print("15");
  lcd.write(0xdf);

  printNumber(m, 4);
  printNumber(c, 7);
  printNumber(d, 10);
  printNumber(u, 13);
} 

String FormatString(String f, String s)
{
    for (int i = 0; i < s.length(); i++) {
      f[i + f.length() - s.length()] = s[i]; 
    }
    return f;
}

//void printEMS(int rpm, int g_altitude) 
void printEMS() 
{
    // Line 0
    lcd.setCursor(0, 0);
		lcd.print(readVcc() / 1000); 	 
    lcd.print("V ");
	
    lcd.setCursor(4, 0);
 		lcd.print(FormatString("   ", String(g_cht)));   // lose the last digit, make it 0

    lcd.setCursor(8, 0);
 		lcd.print(FormatString("   ", String(g_egt)));   // lose the last digit, make it 0

    lcd.setCursor(4, 1);
    lcd.print("---");

    lcd.setCursor(8, 1);
    lcd.print("---");

    lcd.setCursor(12, 0);
 		//lcd.print(FormatString("    ", String(g_rpm / 10 * 10)));   // lose the last digit,, make it 0
 		lcd.print(FormatString("    ", String(g_rpm)));   // lose the last digit,, make it 0
    //String s = String(rpm);
    //lcd.print(s);

    // Line 1
    lcd.setCursor(0, 1);
    //lcd.print((int)bmp.readTemperature());
    lcd.print(g_oat); 
    lcd.write(0xdf);  // degree symbol

    lcd.setCursor(12, 1);
    //g_altitude = 1234;
 		//lcd.print(FormatString("0000", String(g_altitude / 10 * 10)));   //f
 		lcd.print(FormatString("    ", String(g_altitude / 10 * 10)));   //ft

    //lcd.setCursor(8, 1);
    //lcd.print("333");
} 


void printBAR(int col, int rpm, int val) 
{
		//constrain(val, 0, 16);
		val %= 16;

    // Colom 0
    // Line 0
    lcd.setCursor(0, 0);
		lcd.print(readVcc() / 1000); 	 
    lcd.print("V ");

    // Line 1
    lcd.setCursor(0, 1);
    lcd.print(g_oat); 
    lcd.write(0xdf);  // degree symbol

    // Colom last
    lcd.setCursor(12, 0);
    //lcd.print("3250");
    lcd.print(g_rpm);
    //String s = String(rpm);
    //lcd.print(s);

    //////////////////
    // bargraphs
    
    lcd.setCursor(col, 0);
    if (val < 9) lcd.write(0x20);    
    else lcd.write((int)(val / 9) * val);     // 8 bars  + null = 9

    lcd.setCursor(col, 1);
    if (val == 0) lcd.write(0x20);    
    else lcd.write(min(8, val));  
} 

// Vcc in mV



long readVcc() 
{
   return  analogRead(1) * 13.8;      // read the value from the pot 
  //  return -1;  // stub until we decide how to read bus voltage
}

/*
long _readVcc() 
{
  // Read 1.1V reference against AVcc
  // set the reference to Vcc and the measurement to the internal 1.1V reference
  #if defined(__AVR_ATmega32U4__) || defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__)
    ADMUX = _BV(REFS0) | _BV(MUX4) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
  #elif defined (__AVR_ATtiny24__) || defined(__AVR_ATtiny44__) || defined(__AVR_ATtiny84__)
    ADMUX = _BV(MUX5) | _BV(MUX0);
  #elif defined (__AVR_ATtiny25__) || defined(__AVR_ATtiny45__) || defined(__AVR_ATtiny85__)
    ADMUX = _BV(MUX3) | _BV(MUX2);
  #else
    ADMUX = _BV(REFS0) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
  #endif  

  delay(2); // Wait for Vref to settle
  ADCSRA |= _BV(ADSC); // Start conversion
  while (bit_is_set(ADCSRA,ADSC)); // measuring

  uint8_t low  = ADCL; // must read ADCL first - it then locks ADCH  
  uint8_t high = ADCH; // unlocks both

  long result = (high<<8) | low;

  result = 1125300L / result; // Calculate Vcc (in mV); 1125300 = 1.1*1023*1000
  return result; // Vcc in millivolts
}
*/




//-----------------------------------------------------------------------------
// Tacho interrupt service routine
//
void isr() 
{
  g_revticks = g_revticks + 1;
}

void enableInterrupt()
{
  attachInterrupt(digitalPinToInterrupt(INTERRUPT_PIN), isr, RISING);  
  //attachInterrupt(digitalPinToInterrupt(INTERRUPT_PIN), isr, FALLING);   
  //attachInterrupt(digitalPinToInterrupt(INTERRUPT_PIN), isr, CHANGE);  
}

void disableInterrupt()
{
  detachInterrupt(digitalPinToInterrupt(INTERRUPT_PIN));  // pin 3
}


//-----------------------------------------------------------------------------
// Initialization
//
void setup()
{
  Serial.begin(9600);

  // Set up LCD
	lcd.begin(16, 2);  // sets the LCD's rows and colums:
  bmp.begin(BMP085_STANDARD);

  defineUDC_1(); // Create User Defined Character #1
  pinMode(RISING, INPUT_PULLUP);
  enableInterrupt();

  state = 2; // State variable

  // Welcome banner
  lcd.clear();
  lcd.print(" Bienvenue a la");
  lcd.setCursor(0, 1);
  lcd.print("     Luciole     ");
  delay(2000);
  lcd.clear();
}


//-----------------------------------------------------------------------------
// Main execution loop
//
void loop()
{
  // For debugging
	i = 1000 + 10*(millis() / 1000) % 10000; 


	// Handle the buttons
	lcd_key = read_LCD_buttons();  // read the buttons
  
	// Latch the press
	if ((lcd_key != btnNONE) && (lcd_key != _lcd_key)) { 
    lcd_key_pressed = true;
    delay(200);  // allow to settle
  }
	else lcd_key_pressed = false;

 	_lcd_key = lcd_key;
	

	if (lcd_key_pressed) {
		switch (lcd_key) {
		  case btnLEFT:
			case btnDOWN: 
				state--;
				if (state < 1) state = MAX_STATE;
				break;
		  case btnRIGHT: 
			case btnUP: 
				state++; 
				if (state > MAX_STATE) state = 1;
				break;
		}  // switch
		

    switch (state) {
      case 1: 
        defineUDC_0(); 
        break;
      case 2: 
        break;
      case 3: 
        defineUDC_1(); 
        break;
    }
   	lcd.begin(16, 2);
	}


	// Calc Revs and also  Update the screen

  unsigned long elaps_time = millis() - _millis;
  if (elaps_time >= TIMER) {
		disableInterrupt();           // detach the interrupt
    // Calc the revs
    noInterrupts();
		g_rpm = (g_revticks * 60000.0 / elaps_time);   // calculates rpm
    interrupts();

    int rv  = analogRead(1);      // read the value from the pot 
    // Format for display
    //int rpm = (int) (g_rpm / 10) * 10;  // lose the last digit
    g_altitude = (int) (bmp.readAltitude() * 3.28084);  // In feet
    g_oat = (int) bmp.readTemperature();  // in deg C
    g_cht = (int) max6675_0.readCelsius();  // in deg C
    g_egt = (int) max6675_1.readCelsius();  // in deg C
    //g_egt = 123;
    
    switch (state) {
      case 1: printRPM(g_rpm); break;
      case 2: printEMS(/*rpm, g_altitude*/); break;
      case 3: 
        printBAR(4, g_rpm, rv / 64);  //  1024 / 16 = 64
        printBAR(5, g_rpm, rv / 77); 
        printBAR(6, g_rpm, rv / 68); 
        printBAR(7, g_rpm, rv / 72); 
        break;
      //case 2: printBAR(4, i, 8); break;
      //case 3: echoButton(); break;
    }

    /*
    //Serial.print("C = "); 
    //Serial.println(max6675.readCelsius());
    Serial.print("RPM = "); 
    Serial.print(g_rpm);
    Serial.print("  Alt = "); 
    Serial.print(g_altitude);
    Serial.print("  OAT = "); 
    Serial.print(g_oat);
    Serial.print("  TC0 = "); 
    Serial.print(g_cht);
    Serial.print("  TC1 = "); 
    Serial.print(g_egt);
    Serial.println();
    */

		g_revticks = 0;
    enableInterrupt();  // attach the interrupt
    _millis = millis();
  }
}